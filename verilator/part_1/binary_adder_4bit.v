module binary_adder_4bit(
    a,
    b,
    carry,
    sum
);

input [3:0] a;
input [3:0] b;
output carry;
output [3:0] sum;

wire fa_2_carry;
wire fa_1_carry;
wire fa_0_carry;

full_adder fa_3(
    .a(a[3]),
    .b(b[3]),
    .c(fa_2_carry),
    .carry(carry),
    .sum(sum[3])
);

full_adder fa_2(
    .a(a[2]),
    .b(b[2]),
    .c(fa_1_carry),
    .carry(fa_2_carry),
    .sum(sum[2])
);

full_adder fa_1(
    .a(a[1]),
    .b(b[1]),
    .c(fa_0_carry),
    .carry(fa_1_carry),
    .sum(sum[1])
);

full_adder fa_0(
    .a(a[0]),
    .b(b[0]),
    .c(0),
    .sum(sum[0]),
    .carry(fa_0_carry)
);

endmodule;
