module binary_adder_4bit(
    a,
    b,
    carry,
    sum
);

input [3:0] a;
input [3:0] b;
output carry;
output [3:0] sum;

assign sum = {0, 0, 0, 0};

endmodule;
