#include <stdlib.h>
#include "Vbinary_adder_4bit.h"
#include "verilated.h"
#include "verilated_vcd_c.h"

int main(int argc, char **argv) {
    bool res = true;
    uint16_t time = 0;

    // Initialize Verilators variables
    Verilated::commandArgs(argc, argv);

    // Create an instance of our module under test
    Vbinary_adder_4bit *tb = new Vbinary_adder_4bit;

    // Configure VCD tracing.
    Verilated::traceEverOn(true);
    VerilatedVcdC* tfp = new VerilatedVcdC;

    tb->trace(tfp, 99);
    tfp->open("test_4bit_adder.vcd");

    // Begin tests.

    tfp->dump(time);

    time += 1;

    tb->a = 0b0001;
    tb->b = 0b0000;
    tb->eval();
    tfp->dump(time);

    if (tb->sum != 0b0001 || tb->carry) {
        printf("Test 0001 + 0000 failed.\n");
        res = false;
    }

    time += 1;

    tb->a = 0b0001;
    tb->b = 0b0001;
    tb->eval();
    tfp->dump(time);

    if (tb->sum != 0b0010 || tb->carry) {
        printf("Test 0001 + 0001 failed.\n");
        res = false;
    }

    time += 1;

    tb->a = 0b1001;
    tb->b = 0b0011;
    tb->eval();
    tfp->dump(time);

    if (tb->sum != 0b1100 || tb->carry) {
        printf("Test 1001 + 0011 failed.\n");
        res = false;
    }

    time += 1;

    tb->a = 0b1100;
    tb->b = 0b0100;
    tb->eval();
    tfp->dump(time);

    if (tb->sum != 0b0000 || !tb->carry) {
        printf("Test 1100 + 0100 failed.\n");
        res = false;
    }

    time += 1;

    tb->a = 0b1111;
    tb->b = 0b1111;
    tb->eval();
    tfp->dump(time);

    if (tb->sum != 0b1110 || !tb->carry) {
        printf("Test 1111 + 1111 failed.\n");
        res = false;
    }

    if (!res) {
        printf("Tests failed.\n");
    } else {
        printf("Tests passed.\n");
    }

    tfp->close();

	exit(EXIT_SUCCESS);
}
