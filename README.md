Overview
========

*Simulating Digital Circuits with Verilator* is a talk prepared for and given
at the 2019.1 session of the Phoenix Computer Engineering Club. We briefly
introduce boolean algebra, truth tables, and a few digital logic gates. Then we
cover the basic cases of binary addition and the circuit for a 4-bit binary
adder. Then we introduce HDLs in general, VHDL, and Verilog. Finally, we build
the 4-bit binary adder in Verilog and simulate it using Verilator, with a
demonstration of both assertion-based automated tests and waveform
visualization using GTKWave.

Dependencies
============

* GNU Make, GCC, and Verilator to build the demonstration.
* GTKWave to view the VCD output files from the demonstration.
* LibreOffice Impress, if editing the presentation is desired.

Build and Execute
=================

```
cd verilator
make
cd part_0 && ./obj_dir/Vbinary_adder_4bit; cd -
cd part_1 && ./obj_dir/Vbinary_adder_4bit; cd -
```

Copyright and License
=====================

This talk and all associated materials are free (libre).

Copyright 2019 Francis (Fritz) Mahnke

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
